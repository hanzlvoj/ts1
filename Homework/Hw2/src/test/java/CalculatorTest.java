import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {

    Calculator calculator;

    @BeforeEach
    public void singleTestSetup(){
        calculator = new Calculator();
    }

    @Test
    public void add_SumOfNegative5And5_0(){
        int expectedValue = 0;

        int result = calculator.add(-5,5);

        assertEquals(expectedValue, result);
    }

    @Test
    public void subtract_SubtractNegative5From5_10(){
        int expectedValue = 10;

        int result = calculator.subtract(5,-5);

        assertEquals(expectedValue, result);
    }

    @Test
    public void multiply_Multiply10By1_10(){
        int expectedValue = 10;

        int result = calculator.multiply(10,1);

        assertEquals(expectedValue, result);
    }

    @Test
    public void divide_Divide5By5_1() throws Exception {
        int expectedValue = 1;

        int result = calculator.divide(5,5);

        assertEquals(expectedValue, result);
    }

    @Test
    void divide_TestExceptionDivide5By0_Exception() {

        Assertions.assertThrows(Exception.class, () -> {
            calculator.divide(5,0);
        });


    }

}
