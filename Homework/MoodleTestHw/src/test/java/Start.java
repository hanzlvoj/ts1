import io.github.bonigarcia.wdm.WebDriverManager;

public class Start {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        Login login = new Login();
        login.login();
    }
}
