
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {
    WebDriver driver;


    public  Login() {
        this.driver = new ChromeDriver();

    }


    public void login(){
        driver.get("https://moodle.fel.cvut.cz/");


        WebElement login = driver.findElement(By.linkText("Přihlásit se"));
        login.click();
        login = driver.findElement(By.linkText("SSO přihlášení"));
        login.click();

        WebElement username = driver.findElement(By.name("j_username"));
        WebElement pass = driver.findElement(By.name("j_password"));
        username.sendKeys("");
        pass.sendKeys("");
        login = driver.findElement(By.name("_eventId_proceed"));
        login.click();


        driver.get("https://moodle.fel.cvut.cz/mod/quiz/view.php?id=233484");
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section/div[1]/div[5]/div/form/button")).click();
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section/div[1]/form/div[2]/div[2]/fieldset/div/div[1]/span/input")).click();

        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section[1]/div[1]/form/div/div[1]/div[2]/div/div[2]/div[1]/textarea")).sendKeys("Vojtěch Hanzlík 101");
        driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section[1]/div[1]/form/div/div[2]/div[2]/div[1]/div[2]/span/input")).sendKeys("86400");
        Select select = new Select(driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section[1]/div[1]/form/div/div[3]/div[2]/div[1]/div/p/span/select")));
        select.selectByVisibleText("Oberon");
        select = new Select(driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div/section[1]/div[1]/form/div/div[4]/div[2]/div[1]/div/p/span/select")));
        select.selectByVisibleText("Rumunsko");

    }

}