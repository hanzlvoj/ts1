package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import shop.Item;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {

    ItemStock itemStock;

    int id = 2;
    String name = "testItem";
    float price = 104;
    String category = "testCategory";
    Item item = new Item(id,name,price,category);

    @Test
    public void ItemStockTest_argumentsOfTheClassConstructor_dataSetAsTheArguments(){
        itemStock = new ItemStock(item);

        Assertions.assertEquals(item,itemStock.getItem());
        Assertions.assertEquals(0,itemStock.getCount());

    }

    @ParameterizedTest
    @CsvFileSource(resources = "/testChangeCount.csv", numLinesToSkip = 1)

    public void ChangeCountTest(int expected, int result){
        itemStock = new ItemStock(item);
        Assertions.assertEquals(expected, result);
        itemStock.IncreaseItemCount(5);
        Assertions.assertEquals(expected, result);
        itemStock.decreaseItemCount(6);
        Assertions.assertEquals(expected, result);


    }


}