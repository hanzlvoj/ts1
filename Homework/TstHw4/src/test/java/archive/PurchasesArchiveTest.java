package archive;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import shop.Item;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class PurchasesArchiveTest {


    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }
    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }



    int id = 2;
    String name = "testItem";
    float price = 104;
    String category = "testCategory";
    Item item = new Item(id,name,price,category);


    @Test
    void printItemPurchaseStatisticsTest() {
        ItemPurchaseArchiveEntry entry1 = new ItemPurchaseArchiveEntry(item);

        PurchasesArchive purchasesArchive = new PurchasesArchive();
        purchasesArchive.putToItemPurchaseArchive(1 ,entry1);

        purchasesArchive.printItemPurchaseStatistics();
        assertEquals("ITEM PURCHASE STATISTICS:" + "\n" + "ITEM  Item   ID 2   NAME testItem   CATEGORY testCategory   HAS BEEN SOLD 1 TIMES"+"\n" , outContent.toString());


    }

    @Test
    void getHowManyTimesHasBeenItemSoldTest() {
    }

    @Test
    void putOrderToPurchasesArchiveTest() {
    }
}