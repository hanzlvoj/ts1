package shop;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

class StandardItemTest {


    StandardItem standardItem;

    int id = 1;
    String name = "milk";
    float price = 100;
    String category = "dairy";
    int loyaltyPoints = 2;



    @Test
    public void StandardItemTest_argumentsOfTheClassConstructor_dataSetAsTheArguments() {

        standardItem = new StandardItem(id,name,price,category,loyaltyPoints);

        Assertions.assertEquals(id,standardItem.getID());
        Assertions.assertEquals(name,standardItem.getName());
        Assertions.assertEquals(price,standardItem.getPrice());
        Assertions.assertEquals(category,standardItem.getCategory());
        Assertions.assertEquals(loyaltyPoints,standardItem.getLoyaltyPoints());

    }



    @Test
    public void copy_copies_instanceOfStandardItem_copyOfTheInstance() {

        standardItem = new StandardItem(id,name,price,category,loyaltyPoints);

        StandardItem copy = standardItem.copy();

        Assertions.assertEquals(copy,standardItem);
    }


    @ParameterizedTest
    @CsvFileSource(resources = "/testEquals.csv", numLinesToSkip = 1)

    public void testEquals(int id, String name, float price, String category, int loyaltyPoints,
                           int id2, String name2, float price2, String category2, int loyaltyPoints2){

        StandardItem firstItem = new StandardItem(id,name,price,category,loyaltyPoints);
        StandardItem secondItem = new StandardItem(id2,name2,price2,category2,loyaltyPoints2);

        if(firstItem.getID() == secondItem.getID() && firstItem.getName().equals(secondItem.getName()) && firstItem.getPrice() == secondItem.getPrice()
            && firstItem.getCategory().equals(secondItem.getCategory()) && firstItem.getLoyaltyPoints() == secondItem.getLoyaltyPoints()){

            Assertions.assertEquals(firstItem.equals(secondItem),true);


        }else{
            Assertions.assertEquals(firstItem.equals(secondItem),false);
        }

    }

}











