package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {

    int id = 1;
    String name = "milk";
    float price = 100;
    String category = "dairy";

    int id2 = 2;
    String name2 = "testItem";
    float price2 = 104;
    String category2 = "testCategory";


    Item item1 = new Item(id,name,price,category);
    Item item2 = new Item(id2,name2,price2,category2);
    Item item3 = null;


    ArrayList<Item> items = new ArrayList<>();

    String customerName = "testName";
    String customerAddress = "testAddress";
    int state = 1;

    @Test
    public void OrderTest_argumentsOfTheClassConstructor_dataSetAsTheArguments(){

        items.add(item1);
        items.add(item2);
        items.add(item3);

        ShoppingCart shoppingCart = new ShoppingCart(items);
        Order order = new Order(shoppingCart, customerName, customerAddress, state);

        Assertions.assertEquals(items,shoppingCart.getCartItems());
        Assertions.assertEquals(customerName,order.getCustomerName());
        Assertions.assertEquals(customerAddress,order.getCustomerAddress());
        Assertions.assertEquals(state,order.getState());

    }

    @Test
    public void OrderTest_argumentsOfTheClassConstructorWithoutState_dataSetAsTheArguments(){

        items.add(item1);
        items.add(item3);

        ShoppingCart shoppingCart = new ShoppingCart(items);
        Order order = new Order(shoppingCart, customerName, customerAddress);

        Assertions.assertEquals(items,shoppingCart.getCartItems());
        Assertions.assertEquals(customerName,order.getCustomerName());
        Assertions.assertEquals(customerAddress,order.getCustomerAddress());
        Assertions.assertEquals(0,order.getState());

    }


}