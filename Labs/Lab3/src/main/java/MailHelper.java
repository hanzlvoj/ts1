import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class MailHelper {
    private DBManager dbManager;

    public MailHelper(DBManager dbManager){
        this.dbManager = dbManager;
    }

    public void createAndSendMail(String to, String subject, String body)
    {
        Mail mail = new Mail();
        mail.setTo(to);
        mail.setSubject(subject);
        mail.setBody(body);
        mail.setIsSent(false);

        saveMailToDB(mail);
        sendMailAssynchronous(mail.getMailId());
    }

    public void sendMail(int mailId)
    {
        try
        {
            // get entity
            Mail mail = dbManager.findMail(mailId);
            if (mail == null) {
                return;
            }

            if (mail.isSent()) {
                return;
            }

            String from = "user@fel.cvut.cz";
            String smtpHostServer = "smtp.cvut.cz";
            Properties props = System.getProperties();
            props.put("mail.smtp.host", smtpHostServer);
            Session session = Session.getInstance(props, null);
            MimeMessage message = new MimeMessage(session);

            message.setFrom(from);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getTo(), false));
            message.setSubject(mail.getSubject());
            message.setText(mail.getBody(), "UTF-8");

            // send
            Transport.send(message);
            mail.setIsSent(true);
            dbManager.saveMail(mail);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void saveMailToDB(Mail mail){
        dbManager.saveMail(mail);
    }

    protected void sendMailAssynchronous(int mailId){
        if (!Configuration.isDebug) {
            (new Thread(() -> {
                sendMail(mailId);
            })).start();
        }
    }

}
