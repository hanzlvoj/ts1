import org.junit.jupiter.api.Test;
import org.junit.platform.commons.annotation.Testable;

import java.util.ArrayList;
import java.util.List;

public class MailHelperTest {

    @Test
    public void createAndSendMail_ValidEmailParametersPassed_EmailSendTo(){


    }


}

class TestableMailHelper extends MailHelper {
    private List<Mail> mailStorage = new ArrayList<Mail>();
    private int sendMailCounter = 0;


    public TestableMailHelper() {
        super(new DBManager());
    }

    @Override

    protected void saveMailToDB(Mail mail){
        mailStorage.add(mail);
    }

    @Override
    protected void sendMailAssynchronous(int mailId){
        sendMailCounter++;
    }

    public int getSendMailCounter(){
        return sendMailCounter;
    }
    public List<Mail> getMailStorage(){
        return mailStorage;
    }

}