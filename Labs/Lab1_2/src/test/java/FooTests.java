import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class FooTests {
    Foo foo;

    @BeforeEach
    public void singleTestSetUp() {
        foo = new Foo();
    }

    @Test
    public void returnNumber_ReturningSpecificNumber_Return5() {
        int expectedValue = 5;

        int result = foo.returnNumber();

        assertEquals(expectedValue, result);
    }

    @Test
    public void getNum_GetDefaultNumber_Return0() {
        int expectedValue = 0;

        int result = foo.getNum();

        assertEquals(expectedValue, result);
    }

    @Test
    public void Increment_returnGreaterNumber_PlusOne() {
        int expectedValue = foo.getNum() + 1;

        foo.increment();

        assertEquals(expectedValue, foo.getNum());
    }

    @ParameterizedTest(name = "{0} plus {1} should be equal to {2}")
    @CsvSource({"1, 2, 3", "2, 3, 5"})
    @CsvFileSource(resources = "data.csv", numLinesToSkip = 1)
    public void add_addsAandB_returnsC(int a, int b, int c) {
    // arrange

        int expectedResult = c;
    // act
        int result = foo.sum(a, b);
    // assert
        assertEquals(expectedResult, result);
    }

}
